# MPT 文档

##RLP解码
###编码前缀
```c++
static const byte c_rlpMaxLengthBytes = 8;
static const byte c_rlpDataImmLenStart = 0x80; //128 10000000 表示0的时候
static const byte c_rlpListStart = 0xc0; //11000000
static const byte c_rlpDataImmLenCount = c_rlpListStart - c_rlpDataImmLenStart - c_rlpMaxLengthBytes; //192 - 128 - 8 = 56 00111000
static const byte c_rlpDataIndLenZero = c_rlpDataImmLenStart + c_rlpDataImmLenCount - 1; //128  + 56 - 1 = 183 10110111 表示数值自己数时需要用到
static const byte c_rlpListImmLenCount = 256 - c_rlpListStart - c_rlpMaxLengthBytes; //256 - 192 - 8 = 56 00111000
static const byte c_rlpListIndLenZero = c_rlpListStart + c_rlpListImmLenCount - 1; //192 + 56 - 1 = 247 11110111  表示list长度值的字节数
```
###插入数值情况说明

* 插入 1
```
0x 11000001 00000001
`11000000`代表是list `11000001`代表list长度为1  00000001存储的是数值`1`
```
* 插入 0
```
0x 11000001 10000000
`11000000`代表是list `11000001`代表list长度为1  10000000存储的是数值`0` (c_rlpDataImmLenStart)
```
* 插入 128
```
0x 11000010 10000001 10000000
`11000000`代表是list `11000010`代表list长度为2  10000001存储的是数值长度`1`  (c_rlpDataImmLenStart)   `10000000`数值为128  
```
* 插入 1280000000000000
```
0x 11000101 10000100 00111001 01010000 00000000 00000000
`11000000`代表是list `11000100`代表list长度为5  10000100存储的是数值长度`4`  (c_rlpDataImmLenStart)   `00111001 01010000 00000000 00000000`数值为 1280000000000000
```
* 插入 "a"
```
11000001 01100001
11000000`代表是list `11000001`代表list长度为1 `01100001 ` 字符串`a`
```
* 插入 "abc"
```
0x 11000100 10000011 01100001 01100010 01100011
11000000`代表是list `11000011`代表list长度为3 `01100001 01100010 01100011` 字符串`abc`
```

```
`11111000 10000001 10111000 01111111 00000001 00000001 00000010 00000011 00000100 00000101 00000110 00000111 00001000 00001001 00001010 00001011 00001100 00001101 00001110 00001111 00010000 00010001 00010010 00010011 00010100 00010101 00010110 00010111 00011000 00011001 00011010 00011011 00011100 00011101 00011110 00011111 00100000 00100001 00100010 00100011 00100100 00100101 00100110 00100111 00101000 00101001 00101010 00101011 00101100 00101101 00101110 00101111 00110000 00110001 00110010 00110011 00110100 00110101 00110110 00110111 00111000 00111001 00111010 00111011 00111100 00111101 00111110 00111111 01000000 01000001 01000010 01000011 01000100 01000101 01000110 01000111 01001000 01001001 01001010 01001011 01001100 01001101 01001110 01001111 01010000 01010001 01010010 01010011 01010100 01010101 01010110 01010111 01011000 01011001 01011010 01011011 01011100 01011101 01011110 01011111 01100000 01100001 01100010 01100011 01100100 01100101 01100110 01100111 01101000 01101001 01101010 01101011 01101100 01101101 01101110 01101111 01110000 01110001 01110010 01110011 01110100 01110101 01110110 01110111 01111000 01111001 01111010 01111011 01111100 01111101 01111110`
`11111000` 表示list长度需要1字节（c_rlpListIndLenZero(11110111 )+1)  `10000001`item总共长度129  `10111000`字符串长度需要1字节 (c_rlpDataIndLenZero(10110111) +1) `01111111`元素长度 
```

##MPT树
### MPT树有三种类型节点


* 叶子节点,扩展节点
```
    叶子节点与扩展节点相似，分为两部分[key,value]形式。但在'value'方面，叶子节点'value'存储的是真实的数据(RLP编码)，而扩展节点存储的是指向下一个节点hash值
```
* 分支节点
```
    分支节点会有17个元素。因为MPT是以16进制来保存。所以会有16个元素来指向下一个叶子节点或扩展节点。如果有[key,value]在此分支节点终止，则第17个元素用来保存其值。分支节点既可作为中间节点也可作为终止节点
```

###节点示例
由于以太坊代码中的key为sha3后的key, 不好模拟前缀相同的插入情况。所以采用非sha3的示例来说明，便于我们说明理解

* 1.插入新节点
[![1.jpg](https://www.tuchuang001.com/images/2017/05/23/1.jpg)](https://www.tuchuang001.com/image/KhxxE)
* 2.更新节点

[![2.jpg](https://www.tuchuang001.com/images/2017/05/23/2.jpg)](https://www.tuchuang001.com/image/KhdVu)

* 3.插入key(1234)value(abcdef)
[![3.md.png](https://www.tuchuang001.com/images/2017/05/23/3.md.png)](https://www.tuchuang001.com/image/KhmXI)

* 4.插入key(12345)value(abcdef)
[![4.md.jpg](https://www.tuchuang001.com/images/2017/05/23/4.md.jpg)](https://www.tuchuang001.com/image/KhyIa)

* 5.插入key(12)value(ab)
[![5a7c33.jpg](https://www.tuchuang001.com/images/2017/05/23/5a7c33.jpg)](https://www.tuchuang001.com/image/Kz6za)

* 6.插入key(125)value(efg)
[![64ee01.jpg](https://www.tuchuang001.com/images/2017/05/23/64ee01.jpg)](https://www.tuchuang001.com/image/KzpmW)

* 更新流程
    * 1.查找与当前root节点共同前缀， prefix 为共同前缀，remain_key 为新节点非共同前缀， remain_cur_key为root节点非共同前缀部分
    * 2.如果key完全相同且为叶子节点则更新当前root节点(参照2图）
    * 3.如果remain_cur_key为空，也就是新节点key包含root的key。
        * 如果root为扩展节点，则找到扩展节点value指向的节点作为当前root，与remain_key作为新key，回到步骤1；
        * 如果root为叶子节点，则建立新分支节点branch;branch[16]为root节点的value值（也就是说分支节点为一个终止节点），branch[remain_key[0]]保存remain_key[1:]和value的RLP值（参照3图）
    * 4.如果remain_cur_key不为空，则建立分支节点branch。branch[remain_cur_key[0]]保存root节点value信息（如果为root分支节点则保存value, 叶子节点则保存remain_cur_key[1:]和value),
        * 如果remain_key也不空branch[remain_key[0]]保存remain_key[1:]和新节点value（图6）
        * 否则(remain_key为空，新节点到此branch终止)branch[16]保存新节点value值(branch 为一个终止节点)(图5）
    * 5.如果prefix不为空，则prefix作为key, 新节点作为value(前面几布生成的新branch或新扩展节点)
    * 6.如果为分支节点branch，branch[key[0]]为root节点，key[1:]为新key ,递归查找。最终branch[key[0]]保存新生成的节点。(发生在branch节点连接新节点)

* 删除
    * 删除key(1234)
        1.找到根节点
    [![198092.jpg](https://www.tuchuang001.com/images/2017/05/23/198092.jpg)](https://www.tuchuang001.com/image/KlJE4)
        2.由于value是一个分支节点，则找到指定分支节点
            [![2c5fb1.jpg](https://www.tuchuang001.com/images/2017/05/23/2c5fb1.jpg)](https://www.tuchuang001.com/image/Klicn)
        3.key(1234)剩余remain_key[0]为4，则找到对应位置的节点
[![3.jpg](https://www.tuchuang001.com/images/2017/05/23/3.jpg)](https://www.tuchuang001.com/image/KloTs)
        4.节点为叶子节点，找到叶子节点，将其删除
[![456b27.jpg](https://www.tuchuang001.com/images/2017/05/23/456b27.jpg)](https://www.tuchuang001.com/image/KlsL7)
        5.删除叶子节点后，发现分支节点现在只有value值，所以需要调整
            [![5.jpg](https://www.tuchuang001.com/images/2017/05/23/5.jpg)](https://www.tuchuang001.com/image/Klkoj)
        6.需要调整，进行合并value值合并到root节点
            [![6.jpg](https://www.tuchuang001.com/images/2017/05/23/6.jpg)](https://www.tuchuang001.com/image/KlvjJ)
        5.1如果删除后是分支节点剩前缀值，则需要调整
            [![7.jpg](https://www.tuchuang001.com/images/2017/05/23/7.jpg)](https://www.tuchuang001.com/image/KlaZZ)
        6.1调整后
            [![8.jpg](https://www.tuchuang001.com/images/2017/05/23/8.jpg)](https://www.tuchuang001.com/image/KlcC6)
        5.2如果删除后分支节点是剩前缀值，且节点对应位置有其它分支
            [![9.jpg](https://www.tuchuang001.com/images/2017/05/23/9.jpg)](https://www.tuchuang001.com/image/KlThP)
        6.2调整后
            [![10.jpg](https://www.tuchuang001.com/images/2017/05/23/10.jpg)](https://www.tuchuang001.com/image/KlIgI)
    1.当前root节点为叶子节点，key为删除节点的key直接删除。
    2.当前root节点为扩展节点，找到root节点value指向的节点，递归查找要删除的节点
    3.当前root节点为分支节点branch，找到branch[key[0]]执行的节点，和剩余key[1:]，递归删除




###以太坊代码说明
    
```
 //首字节表明是叶子节点以及长度的奇偶性
//00001 为奇数扩展节点 0000 偶数扩展节点 0010 偶数叶子节点 0011 奇数叶子节点
偶数节点需要增加4位0000来
std::string ret(1, ((_leaf ? 2 : 0) | (odd ? 1 : 0)) * 16);

inline byte nibble(bytesConstRef _data, unsigned _i)
{
    //奇数为低4位 偶数为高4位
	return (_i & 1) ? (_data[_i / 2] & 15) : (_data[_i / 2] >> 4);
}

byte n = nibble(_data, i);
if (d & 1)	// odd
	ret.back() |= n;		// or the nibble onto the back 存低4位
else
	ret.push_back(n << 4);	// push the nibble on to the back << 4 高4位
```
1. 空节点
    
```
key:[56e81f171bcc55a6ff8345e692c0f86e5b48e01b996cadc001622fb5e363b421]=>value:[0x80]
```




2.插入新节点
插入 `key:sha3(123)`,`value:RLP("123")`
```
key:[56ed35057aa651964e845b32e856909aa0a59dbc9262505a8b7079bb2249aab2]=>value:[e7a1205569044719a1ec3b04d0afa9e7a5310c7c0473331d13dc9fafe143b2c4e8148a8483313233]
```
我们可到MPT树里`value`存储了实际的`key`,`value`
```
[e7a1205`[569044719a1ec3b04d0afa9e7a5310c7c0473331d13dc9fafe143b2c4e8148a]`8483`[313233]`]
```
其中
`569044719a1ec3b04d0afa9e7a5310c7c0473331d13dc9fafe143b2c4e8148a`为 `sha3(key)`
`[313233]`为`RLP(value)`
原来的树是空，则插入的节点为叶子节点。key为sha3(value)

```
template <class DB> void GenericTrieDB<DB>::insert(bytesConstRef _key, bytesConstRef _value)
{
	std::string rootValue = node(m_root);
	assert(rootValue.size());
	//插入一个节点
	bytes b = mergeAt(RLP(rootValue), m_root, NibbleSlice(_key), _value);

	// mergeAt won't attempt to delete the node if it's less than 32 bytes
	// However, we know it's the root node and thus always hashed.
	// So, if it's less than 32 (and thus should have been deleted but wasn't) then we delete it here.
	if (rootValue.size() < 32)
		forceKillNode(m_root);
	m_root = forceInsertNode(&b);
}
```


```
template <class DB> bytes GenericTrieDB<DB>::mergeAt(RLP const& _orig, h256 const& _origHash, NibbleSlice _k, bytesConstRef _v, bool _inLine)
{

	// The caller will make sure that the bytes are inserted properly.
	// - This might mean inserting an entry into m_over
	// We will take care to ensure that (our reference to) _orig is killed.

	// Empty - just insert here
	if (_orig.isEmpty())
		return place(_orig, _k, _v);

	unsigned itemCount = _orig.itemCount();
	assert(_orig.isList() && (itemCount == 2 || itemCount == 17)); //叶子和扩展节点为2，分支节点为17
	if (itemCount == 2)
	{
		// pair...
		NibbleSlice k = keyOf(_orig); //NibbleSlice 封装奇偶性,叶子扩展标识，以及截取

		// exactly our node - place value in directly.
		if (k == _k && isLeaf(_orig)) //新插入key和root的key相同，直接替换
			return place(_orig, _k, _v);

		// partial key is our key - move down.
		if (_k.contains(k) && !isLeaf(_orig))新插入key包含root的key，并且为非页节点
		{
			if (!_inLine)
				killNode(_orig, _origHash);
			RLPStream s(2);
			s.append(_orig[0]);
			mergeAtAux(s, _orig[1], _k.mid(k.size()), _v);//_orig[1]指向下一个节点， k.mid(k.size()),v)为不相同的key值部分
			return s.out();
		}

		auto sh = _k.shared(k); //返回相同的前缀
		if (sh)
		{
			// shared stuff - cleve at disagreement.
			auto cleved = cleve(_orig, sh);//_orig不相同的前缀的部分
			return mergeAt(RLP(cleved), _k, _v, true);
		}
		else
		{
			//没有相同的建立分支
			// nothing shared - branch
			auto branched = branch(_orig);
			return mergeAt(RLP(branched), _k, _v, true);
		}
	}
	else
	{
		// branch...

		// exactly our node - place value.
		if (_k.size() == 0)
			return place(_orig, _k, _v);

		// Kill the node.
		if (!_inLine) //分支节点17位置存的是value值
			killNode(_orig, _origHash);

		// not exactly our node - delve to next level at the correct index.
		byte n = _k[0];
		RLPStream r(17);
		for (byte i = 0; i < 17; ++i)
			if (i == n) //将新的前缀存入指定下标位置
				mergeAtAux(r, _orig[i], _k.mid(1), _v);
			else
				r.append(_orig[i]);
		return r.out();
	}

}
```
```
template <class DB> void GenericTrieDB<DB>::mergeAtAux(RLPStream& _out, RLP const& _orig, NibbleSlice _k, bytesConstRef _v)
{
	RLP r = _orig;
	std::string s;
	// _orig is always a segment of a node's RLP - removing it alone is pointless. However, if may be a hash, in which case we deref and we know it is removable.
	bool isRemovable = false;
	//_orig存储的是执行下一个节点的hash，所以r.isList为false
	//这种情况出现在分支节点，对应位置不为空，也就是说指向其他节点
	if (!r.isList() && !r.isEmpty())
	{
	    //找到扩展节点的value
		s = node(_orig.toHash<h256>());
		r = RLP(s);
		assert(!r.isNull());
		isRemovable = true;
	}
	bytes b = mergeAt(r, _k, _v, !isRemovable);
	streamNode(_out, b);
}
```



```
template <class DB> bytes GenericTrieDB<DB>::deleteAt(RLP const& _orig, NibbleSlice _k)
{
	// The caller will make sure that the bytes are inserted properly.
	// - This might mean inserting an entry into m_over
	// We will take care to ensure that (our reference to) _orig is killed.

	// Empty - not found - no change.
	if (_orig.isEmpty())
		return bytes();

	assert(_orig.isList() && (_orig.itemCount() == 2 || _orig.itemCount() == 17));
	if (_orig.itemCount() == 2)
	{
		// pair...
		NibbleSlice k = keyOf(_orig);

		// exactly our node - return null.
		if (k == _k && isLeaf(_orig)) //叶子节点并且key相同， 直接删除
		{
			killNode(_orig);
			return RLPNull;
		}

		// partial key is our key - move down.
		if (_k.contains(k)) //key包含
		{
			RLPStream s;
			s.appendList(2) << _orig[0];
			if (!deleteAtAux(s, _orig[1], _k.mid(k.size())))//去掉相同前缀，递归删除
				return bytes(); //没找到返回空
			killNode(_orig);
			RLP r(s.out());
			if (isTwoItemNode(r[1]))
				return graft(r);
			return s.out();
		}
		else
			// not found - no change.
			return bytes();
	}
	else
	{
		// branch...

		// exactly our node - remove and rejig.
		if (_k.size() == 0 && !_orig[16].isEmpty()) //分支节点为终止节点_orig[16]存储的就是value
		{
			// Kill the node.
			killNode(_orig);

			byte used = uniqueInUse(_orig, 16); //分支节点还有没有其它节点
			if (used != 255)
				if (isTwoItemNode(_orig[used]))//扩展或叶子节点
				{
					auto merged = merge(_orig, used);
					return graft(RLP(merged));
				}
				else
					return merge(_orig, used);
			else //为空
			{
				RLPStream r(17);
				for (byte i = 0; i < 16; ++i)
					r << _orig[i];
				r << "";
				return r.out();
			}
		}
		else
		{
		    //递归删除
			// not exactly our node - delve to next level at the correct index.
			RLPStream r(17);
			byte n = _k[0];
			for (byte i = 0; i < 17; ++i)
				if (i == n)
					if (!deleteAtAux(r, _orig[i], _k.mid(1)))	// bomb out if the key didn't turn up.
						return bytes();
					else {}
				else
					r << _orig[i];

			// Kill the node.
			killNode(_orig);

			// check if we ended up leaving the node invalid.
			RLP rlp(r.out());
			byte used = uniqueInUse(rlp, 255);
			if (used == 255)	// no - all ok.
				return r.out();

			// yes; merge
			if (isTwoItemNode(rlp[used]))
			{
				auto merged = merge(rlp, used);
				return graft(RLP(merged));
			}
			else
				return merge(rlp, used);
		}
	}

}
```
